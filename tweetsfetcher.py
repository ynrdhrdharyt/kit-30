import argparse
import requests
import html
import re
import time
import sys
import os
import json
import tweepy

# constants
C_VERSION           = 1
C_APPNAME           = 'tweetsfetcher'
C_AUTHOR            = 'Mahmoud Khonji <m@khonji.org>'
C_YEAR              = '2016'
C_CONSUMER_KEY      = "?????????????????????????" # add your twitter keys
C_CONSUMER_SECRET   = "??????????????????????????????????????????????????"
C_ACCESS_KEY        = "??????????????????????????????????????????????????"
C_ACCESS_SECRET     = "?????????????????????????????????????????????"
C_SLEEP_TWEEPY      = 60*15
C_STATE_DONE        = 0
C_STATE_WORKING     = 1

# save tweet to a file
def savetweet(outputdir, account, tweet):
    
    fpath = '%s/%s/%s-%s.txt' % (outputdir, account,
                                 tweet.created_at,
                                 tweet.id_str)
    fpath = re.sub('\s', '_', fpath)
    fpath = re.sub(':', '-', fpath)

    if not os.path.exists(os.path.dirname(fpath)):
        os.makedirs(os.path.dirname(fpath))

    with open(fpath, 'w') as f:
        f.write(tweet.text + '\n')

# parse arguments
parser = argparse.ArgumentParser()
parser.add_argument("outputdir", help="tweets storage directory.",
                    type=str)
parser.add_argument("account", help="the name of the twitter account.",
                    type=str)
args = parser.parse_args()

# print settings
outputdir = args.outputdir
account = args.account
sys.stderr.write('%s v%s - %s (%s)\n\n'
                 % (C_APPNAME, C_VERSION, C_AUTHOR, C_YEAR))
sys.stderr.write('settings:\n')
sys.stderr.write(' account   : %s\n' % (account))
sys.stderr.write(' output dir: %s\n' % (outputdir))
sys.stderr.write('\n')

# authorize twitter, initialize tweepy
auth = tweepy.OAuthHandler(C_CONSUMER_KEY, C_CONSUMER_SECRET)
auth.set_access_token(C_ACCESS_KEY, C_ACCESS_SECRET)
api = tweepy.API(auth)

# get list of tweets after tweet
sys.stderr.write('downloading..')
sys.stderr.flush()
time_start = time.time()

# keep grabbing tweets until there are no tweets left to grab
state = C_STATE_WORKING
max_id = None
oldest = None
while state == C_STATE_WORKING:
    # all subsequent requests use the max_id param to prevent duplicates
    error_free_fetch = False
    while error_free_fetch == False:
        try:
            tweets = api.user_timeline(screen_name = account,
                                       count=200, max_id=oldest)
        except tweepy.error.TweepError:
            sys.stderr.write('tweepyerror happened. sleeping %s seconds..' %
                             C_SLEEP_TWEEPY)
            sys.stderr.flush()
            time.sleep(C_SLEEP_TWEEPY)
            sys.stderr.write(' ok\n')
            sys.stderr.flush()
        else:
            error_free_fetch = True

    if len(tweets) > 0:
        # store tweet
        for tweet in tweets:
            savetweet(outputdir, account, tweet)

        # ui
        sys.stderr.write(' %s' % len(tweets))
        sys.stderr.flush()

        # advance pointer
        oldest = tweets[-1].id - 1

    else:
        state = C_STATE_DONE

time_end = time.time()
sys.stderr.write(' complete! (%s seconds)\n' % (time_end - time_start))
