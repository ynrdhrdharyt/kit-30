# this script combines the tweets into multiple chunks per author.

import sys
import os
import re
import math

def openfile(f):
    with open(f, 'r') as fh:
        t = fh.read()
        return t

def cleantext(t):
    t = re.sub('@[^\s@]+', '@NAME', t)
    t = re.sub('#[^\s#]+', '#TAG', t)
    t = re.sub('https?://\S+', 'http://URL', t)
    return t

def statstext(t):
    n_letters = len(t)
    n_words = len(t.split())
    return (n_letters, n_words)

def walk (dir_root, dir_out, chunks_num):
    global author_id
    dir_list = os.listdir(dir_root)
    tweets = {}

    for i in dir_list:
        i_path = '%s/%s' % (dir_root, i)

        if os.path.isdir(i_path) == True:
            walk(i_path, dir_out, chunks_num)

        else:
            t = cleantext(openfile(i_path))
            tweets[i] = t

    if tweets:
        x = re.search(r'.*\/', dir_root)
        author_id = dir_root[x.end():]
        chunk_id = 1
        chunk_empty = True
        if not os.path.exists('%s/%s' % (dir_out,author_id)):
            os.makedirs('%s/%s' % (dir_out,author_id))
        fh = open('%s/%s/%.2d.txt' % (dir_out,author_id,chunk_id), 'w')
        c = 0
        for i in sorted(tweets):
            fh.write(tweets[i])
            chunk_empty = False

            c += 1
            if c % math.ceil(len(tweets)/chunks_num) == 0:
                fh.close()
                chunk_id += 1
                fh = open('%s/%s/%.2d.txt' % (dir_out,author_id,chunk_id), 'w')
                chunk_empty = True

        if chunk_empty == True:
            os.remove(fh.name)

        if fh.closed == False:
            fh.close()

# get args
dir_root = sys.argv[1]
dir_out = sys.argv[2]
chunks_num = 10 # total number of chunks per user

# print settings
print('input directory : %s' % dir_root)
print('output directory: %s' % dir_out)
print('chunks per user : %s' % chunks_num)

walk(dir_root, dir_out, chunks_num)
